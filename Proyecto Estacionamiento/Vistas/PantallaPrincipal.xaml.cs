﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClasesBase;

namespace Vistas
{
    /// <summary>
    /// Lógica de interacción para PantallaPrincipal.xaml
    /// </summary>
    public partial class PantallaPrincipal : Window
    {
        public  Usuario usuarioLogin = new Usuario();

       
        

        public PantallaPrincipal()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (usuarioLogin.Rol == "Operador")
            {
                item2.IsEnabled = false;
                item4.IsEnabled = false;
                item5.IsEnabled = false;

            }else if(usuarioLogin.Rol == "Administrador"){

                item1.IsEnabled = false;
                item3.IsEnabled = false;
            }

        }

        private void item3_Click(object sender, RoutedEventArgs e)
        {
            GestionDeEstacionamiento GE = new GestionDeEstacionamiento();
            GE.usuarioGE = usuarioLogin;
            GE.Show();
        }

        private void item1_Click(object sender, RoutedEventArgs e)
        {
            AbmClientes abmCliente = new AbmClientes();
            abmCliente.Show();
        }

        private void item4_Click(object sender, RoutedEventArgs e)
        {
            AbmTipoDeVehiculos abmTipovehiculos = new AbmTipoDeVehiculos();

            abmTipovehiculos.Show();
        }

        private void item2_Click(object sender, RoutedEventArgs e)
        {
            listadoSectoresOcupado ge = new listadoSectoresOcupado();
           // ge.usuarioGE = usuarioLogin;
            ge.Show();
        }

        private void item5_Click(object sender, RoutedEventArgs e)
        {
            ABMUsuarios usuarios = new ABMUsuarios();
            usuarios.Show();
        }

        private void item6_Click(object sender, RoutedEventArgs e)
        {
            ListadoVentas lv = new ListadoVentas();
            lv.Show();
            
        }

        private void item7_Click(object sender, RoutedEventArgs e)
        {
            AcercaDe aD = new AcercaDe();
            aD.Show();
        }

        

       
    }
}
