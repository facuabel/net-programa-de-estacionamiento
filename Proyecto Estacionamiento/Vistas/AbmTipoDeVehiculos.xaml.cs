﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClasesBase;
using System.Diagnostics;
using Microsoft.Win32;
namespace Vistas
{
    /// <summary>
    /// Lógica de interacción para AbmTipoDeVehiculos.xaml
    /// </summary>
    public partial class AbmTipoDeVehiculos : Window
    {
        public AbmTipoDeVehiculos()
        {
            InitializeComponent();
        }

        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {

            TipoVehiculo oTipoVehiculo;

            oTipoVehiculo = new TipoVehiculo();

            oTipoVehiculo.Descripcion = txtDescripcion.Text;
            oTipoVehiculo.Tarifa = System.Convert.ToInt32(txtTarifa.Text);
            oTipoVehiculo.Imagen = txtRuta.Text;

            if (MessageBox.Show("¿Seguro que desea Guardar los datos?", "Confirmacion", MessageBoxButton.YesNo, MessageBoxImage.Question) == (System.Windows.MessageBoxResult.Yes))
            {

                TrabajarTipoVehiculos.guardarTipoV(oTipoVehiculo);

                MessageBox.Show("Se agregó un nuevo registro!");

            }
            else
            {

                MessageBox.Show("Cancelado");
            };

        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void btnExaminar_Click(object sender, RoutedEventArgs e)
        {
             if (imagenTV.Source == null)

    {

        OpenFileDialog openFile = new OpenFileDialog();

        BitmapImage b = new BitmapImage();

        openFile.Title = "Seleccione la Imagen a Mostrar";

        openFile.Filter = "Todos(*.*)|*.*|Imagenes|*.jpg;*.gif;*.png;*.bmp";

        if (openFile.ShowDialog() == true)

        {

            b.BeginInit();

            b.UriSource = new Uri(openFile.FileName);
            txtRuta.Text = b.UriSource.ToString();

            b.EndInit();

            imagenTV.Stretch = Stretch.Fill;

            imagenTV.Source = b;

 
             btnExaminar.Content = "Quitar Foto";
            

        }

    }

    else

    {

        imagenTV.Source = null;

        btnExaminar.Content = "Agregar Foto";

    }




            
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            GestionTiposVehiculos gs = new GestionTiposVehiculos();
            gs.Show();
        }

       
    }
}
