﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClasesBase;

namespace Vistas
{
    /// <summary>
    /// Lógica de interacción para ZonaB.xaml
    /// </summary>
    public partial class ZonaNorte : Window
    {
        DateTime fechaReferecia = new DateTime(2019,09,11,15,00,00);
        string montoTotalEnviar;
        string horasEnviar;
        string tiempoTranscurridoEnviar;
        int zonaEnviar = 2;

       // public Usuario usuarioZN = new Usuario();

        private Usuario usuarioZN;

        public Usuario UsuarioZN
        {
            get { return usuarioZN; }
            set { usuarioZN = value; }
        }
        public ZonaNorte()
        {
            InitializeComponent();
        }

        
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            //if (button1.Background == Brushes.Green)
            //{
            //    MessageBox.Show("Sector Disponible! Registrar Entrada");
            //    button1.Background = Brushes.Red;
            //    int codigoSector = 1;
            //    RegistrarEntrada rg = new RegistrarEntrada();
            //    rg.SectorCodigo = codigoSector;
            //    rg.Show();
                
                
            //}
            //else if (button1.Background == Brushes.Red)
            //{
            //    MessageBox.Show("Sector Ocupado! Registrar Salida");
            //    button1.Background = Brushes.Green;

            //}
            //else if (button1.Background == Brushes.Gray)
            //{
            //    MessageBox.Show("Sector Deshabilitado!");

            //}
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            //Button b = sender as Button;
            // int codigo = Int32.Parse(b.Content.ToString());
            // MessageBox.Show(codigo.ToString());
         
          ////  b = new Button();
          //  if (b.Background == Brushes.Green)
          //  {
          //      MessageBox.Show("Sector Disponible! Registrar Entrada");
          //      b.Background = Brushes.Red;
          //      int codigoSector = Int32.Parse(b.Content.ToString());
          //      RegistrarEntrada rg = new RegistrarEntrada();
          //      rg.SectorCodigo = codigoSector;
          //      rg.Show();


          //  }
          //  else if (b.Background == Brushes.Red)
          //  {
          //      MessageBox.Show("Sector Ocupado! Registrar Salida");
          //      b.Background = Brushes.Green;

          //  }
          //  else if (b.Background == Brushes.Gray)
          //  {
          //      MessageBox.Show("Sector Deshabilitado!");

          //  }
        }

        private void button_ToolTipOpening(object sender, ToolTipEventArgs e)
        {
            
            Button b = sender as Button;
            if (b.Background == Brushes.Green)
            {
                //Calculo del tiempo libre
                int codigo = Int32.Parse(b.Content.ToString());
                DateTime fechaHora = TrabajarTicket.fechaHoraSector(codigo);
                DateTime fechaHoy = DateTime.Now;
                DateTime fechaOtra = fechaHora;

                TimeSpan totalHorasT = fechaHoy.Subtract(fechaOtra);
                string totaldias = totalHorasT.ToString("%d") + " dias";
                string totalHoras = totalHorasT.ToString(@"hh\:mm\:ss");


                b.ToolTip = "Tiempo Desocupado " + totaldias + totalHoras;
            }
            else if(b.Background == Brushes.Red){

                int codigo = Int32.Parse(b.Content.ToString());
                DateTime fechaEntrada = TrabajarTicket.fechaHoraEntrada(codigo);
                DateTime fechaHoy = DateTime.Now;
                TimeSpan totalHoras = fechaHoy - fechaEntrada;



                //tiempo que lleva ocupado
                int resultadoHoras = Convert.ToInt32(totalHoras.Hours.ToString());
                
                int dias = Convert.ToInt32(totalHoras.Days.ToString());
                int minutos = Convert.ToInt32(totalHoras.Minutes.ToString());

                TipoVehiculo tipoV = new TipoVehiculo();

                tipoV = TrabajarTicket.traerTipoVehiculo(codigo);
                int tarifa = Convert.ToInt32(tipoV.Tarifa);
            // convertir los dias en horas
                int horasDias = (dias * 24) + resultadoHoras;

                horasEnviar = horasDias.ToString();
                //montoTotal
                int monto = horasDias * tarifa;
                //monto para el tool
                string montoTotal = monto.ToString();


                tiempoTranscurridoEnviar = "Dias: " +dias.ToString() + " Horas: " +resultadoHoras.ToString() + " Minutos: "+minutos.ToString();
                montoTotalEnviar = montoTotal;


                b.ToolTip = "Tiempo Ocupado: Dias" + dias + " Horas:"+ resultadoHoras
                            + " horas  " + ('\n') + "Tarifa Total: $" + montoTotal + ",00"+('\n') 
                            + "Tipo de Vehiculo: " + tipoV.Descripcion + ('\n')
                            + "Tarifa Aplicada: " + tarifa.ToString();


            }
            else if (b.Background == Brushes.Gray)
            {
                b.ToolTip = "Sector no disponible!";
            }

           
        }

        private void button1_Click_1(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;

            if (b.Background == Brushes.Green)
            {
                MessageBox.Show("Sector Disponible! Registrar Entrada");
                b.Background = Brushes.Red;
                int codigoSector = Int32.Parse(b.Content.ToString());
                RegistrarEntrada rg = new RegistrarEntrada();
                rg.SectorCodigo = codigoSector;

                rg.ZonaNro1 = zonaEnviar;
                rg.usuarioRE = usuarioZN;
                rg.Show();

              

            }
            else if (b.Background == Brushes.Red)
            {
                MessageBox.Show("Sector Ocupado! Registrar Salida");
                RegistrarSalida rs = new RegistrarSalida();
                int codigoSector = Int32.Parse(b.Content.ToString());
                DateTime fechaEntrada = TrabajarTicket.fechaHoraEntrada(codigoSector);

                string nombreCliente = TrabajarTicket.traerNombreCompleto(codigoSector);
                string patente = TrabajarTicket.traerPatente(codigoSector);
                TipoVehiculo tipoV = TrabajarTicket.traerTipoVehiculo(codigoSector);
                int nroTicket = TrabajarTicket.traerNroTicket(codigoSector);

                rs.ticketSal.FechaHoraEnt = fechaEntrada;
                rs.ticketSal.SectorCodigo = codigoSector;
                rs.NombreClienteSal = nombreCliente;
                rs.PatenteSal = patente;
                rs.tipoVSal = tipoV;
                rs.MontoTotalSal = montoTotalEnviar;
                rs.tiempotranscurrido = tiempoTranscurridoEnviar;
                rs.HorasSal = horasEnviar;
                rs.ticketSal.TicketNro = nroTicket;

                rs.usuarioRS = usuarioZN;
                rs.ZonaNro = 2;
                rs.Show();
                b.Background = Brushes.Green;

            }
            else if (b.Background == Brushes.Gray)
            {
                MessageBox.Show("Sector Deshabilitado!");

            }
        }

       
    
       
    }
}
