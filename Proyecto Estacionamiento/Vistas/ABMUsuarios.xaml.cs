﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using ClasesBase;
using System.Collections.ObjectModel;

namespace Vistas
{
    /// <summary>
    /// Lógica de interacción para ABMUsuarios.xaml
    /// </summary>
    public partial class ABMUsuarios : Window
    {
        public ABMUsuarios()
        {
            InitializeComponent();
        }

        ObservableCollection<Usuario> MiLista;
        CollectionView vista;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            MiLista = ((ObjectDataProvider)this.Resources["MisUsuarios"]).Data as ObservableCollection<Usuario>;
            vista = CollectionViewSource.GetDefaultView(MiLista) as CollectionView;

        }

        private void btnPrimero_Click(object sender, RoutedEventArgs e)
        {
            vista.MoveCurrentToFirst();
        }

        private void btnAnterior_Click(object sender, RoutedEventArgs e)
        {
            vista.MoveCurrentToPrevious();
            if (vista.IsCurrentBeforeFirst)
            {
                vista.MoveCurrentToLast();
            }
        }

        private void btnSiguiente_Click(object sender, RoutedEventArgs e)
        {
            vista.MoveCurrentToNext();
            if (vista.IsCurrentAfterLast)
            {
                vista.MoveCurrentToFirst();
            }
        }

        private void btnUltimo_Click(object sender, RoutedEventArgs e)
        {
            vista.MoveCurrentToLast();
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            string userNameEliminar = txtUserName.Text;
            if (MessageBox.Show("¿Desea eliminar el usuario?", "Confirmar!", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                //Se elimina de la base de datos
                TrabajarUsuarios.EliminarUsuario(userNameEliminar);
                //se elimina de la collection
                Usuario oUsuario = vista.CurrentItem as Usuario;
                if (oUsuario != null)
                {
                    MiLista.Remove(oUsuario);
                }
                //vista.Refresh();
            }
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("¿Desea guardar los cambios?", "Confirmar!", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                Usuario usuarioModificar = new Usuario();

                usuarioModificar.Apellido = txtApellido.Text;
                usuarioModificar.Nombre = txtNombre.Text;
                usuarioModificar.Password = txtPassword.Text;
                usuarioModificar.Rol = txtRol.Text;
                usuarioModificar.UserName = txtUserName.Text;
                //se modifica en la base de datos
                TrabajarUsuarios.ModificarUsuario(usuarioModificar);
                //se modifica en la collection
                foreach (Usuario u in MiLista) {
                    if (u.UserName == usuarioModificar.UserName) {

                        u.Nombre = txtNombre.Text;
                        u.Apellido = txtApellido.Text;
                        u.Password = txtPassword.Text;
                        u.Rol = txtRol.Text;
                        u.UserName = txtUserName.Text;

                    }
                } 
                
            }
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("¿Desea agregar un nuevo usuario?", "Confirmar!", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                Usuario usuarioNuevo = new Usuario();
                usuarioNuevo.Apellido = txtApellidoNuevo.Text;
                usuarioNuevo.Nombre = txtNombreNuevo.Text;
                usuarioNuevo.Rol = txtRolNuevo.Text;
                usuarioNuevo.Password = txtPasswordNuevo.Text;
                usuarioNuevo.UserName = txtUserNameNuevo.Text;
                //Se guarda en la Bd
                TrabajarUsuarios.AgregarUsuario(usuarioNuevo);
                //Volver a cargar la collection
            }

        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Listado_de_Usuarios listUsers = new Listado_de_Usuarios();
            listUsers.Show();
        }

       
            

       


    }
}
