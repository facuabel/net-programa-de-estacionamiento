﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClasesBase;


namespace Vistas
{
    /// <summary>
    /// Lógica de interacción para RegistrarSalida.xaml
    /// </summary>
    public partial class RegistrarSalida : Window

    {
        private int zonaNro;

        public int ZonaNro
        {
            get { return zonaNro; }
            set { zonaNro = value; }
        }
       public Ticket ticketSal = new Ticket();
       public TipoVehiculo tipoVSal = new TipoVehiculo();
       public string tiempotranscurrido;
       private string horasSal;
       public Usuario usuarioRS = new Usuario();

       public string HorasSal
       {
           get { return horasSal; }
           set { horasSal = value; }
       }

       string montoTotalSal;

       public string MontoTotalSal
       {
           get { return montoTotalSal; }
           set { montoTotalSal = value; }
       }

       private string nombreClienteSal;

       public string NombreClienteSal
       {
           get { return nombreClienteSal; }
           set { nombreClienteSal = value; }
       }


       private string patenteSal;

       public string PatenteSal
       {
           get { return patenteSal; }
           set { patenteSal = value; }
       }
        public RegistrarSalida()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            labelFechaHoraIngreso.Content = ticketSal.FechaHoraEnt;
            labelFechaSalida.Content = DateTime.Now.ToString();
            txtSector.Text = ticketSal.SectorCodigo.ToString();
            LabelCliente.Content = nombreClienteSal;
            txtPatente.Text = patenteSal;
            labelTipoVehiculo.Content = tipoVSal.Descripcion;
            labelImporte.Content = montoTotalSal;
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            VistaPreviaSalida vs =  new VistaPreviaSalida();

            vs.labelNroticket.Content = ticketSal.TicketNro;
            vs.labelFechaIngreso.Content = labelFechaHoraIngreso.Content;
            vs.labelFechaSalida.Content = labelFechaSalida.Content;
            vs.txtSector.Text = txtSector.Text;
            vs.txtNombreCliente.Text = LabelCliente.Content.ToString();
            vs.txtPatente.Text = txtPatente.Text;
            vs.txtTipoVehiculo.Text = labelTipoVehiculo.Content.ToString();
            vs.txtTarifa.Text = tipoVSal.Tarifa.ToString();
            vs.labelTiempoTranscurrido.Content = tiempotranscurrido;
            vs.labelTotal.Content = montoTotalSal;
            vs.LabelCantidadHoras.Content = horasSal;
            string user = TrabajarTicket.traerNombreUsuario(Convert.ToInt32(txtSector.Text));
            vs.txtNombreUsuario.Text = user;   

            //Guardamos la salida 
            Ticket ticketUpdate = new Ticket();
            ticketUpdate.FechaHoraSal = DateTime.Parse(labelFechaSalida.Content.ToString());
            ticketUpdate.TicketNro = ticketSal.TicketNro;
            ticketUpdate.Duracion = tiempotranscurrido;
            ticketUpdate.Total = Convert.ToInt32(montoTotalSal);

            //se cambia el estado de sector
            string estado = "true";
            int sector = Int32.Parse(txtSector.Text);            
            TrabajarSectores.cambiarEstado(sector, zonaNro,estado);
           TrabajarTicket.actualizarTicket(ticketUpdate);

            vs.Show();




        }




    }
}
