﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


using ClasesBase;
using System.Data;

namespace Vistas
{
    /// <summary>
    /// Lógica de interacción para ListadoVentas.xaml
    /// </summary>
    public partial class ListadoVentas : Window
    {
        public ListadoVentas()
        {
            InitializeComponent();
        }
        DataTable dtglobal = new DataTable();
        private void button1_Click(object sender, RoutedEventArgs e)
        {

            DateTime desde = new DateTime();
            DateTime hasta = new DateTime();

             desde = dateDesde.SelectedDate.Value;
             string desde2 = desde.ToString("yyyyMMdd");
             hasta = dateHasta.SelectedDate.Value;
             string hasta2 = hasta.ToString("yyyyMMdd");

            
            DataTable dt = new DataTable();
            dt = TrabajarTicket.TraerVentas(desde2, hasta2);
            dtglobal = dt;
            listView1.ItemsSource = dt.DefaultView;

            var ids = dt.AsEnumerable().Select(r => r.Field<int>("total")).ToList();
            int sum=0;
            foreach (int t in ids)
            {
                sum += t;
            }

            lbltotal.Content = sum.ToString();

        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            VistaPreviaVenta vs = new VistaPreviaVenta();
            vs.listView1.ItemsSource = dtglobal.DefaultView;
            vs.lblDesde.Content = dateDesde.SelectedDate.Value.ToString();
            vs.lblHasta.Content = dateDesde.SelectedDate.Value.ToString();
            vs.lbltotal.Content = lbltotal.Content;
            vs.Show();

        }
    }
}
