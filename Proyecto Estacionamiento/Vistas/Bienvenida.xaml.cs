﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClasesBase;

namespace Vistas
{
    /// <summary>
    /// Lógica de interacción para Bienvenida.xaml
    /// </summary>
    public partial class Bienvenida : Window
    { 

        public Bienvenida()
        {
            InitializeComponent();
        }

        private void btnIngresar_Click(object sender, RoutedEventArgs e)
        {
            Usuario usuario = new Usuario();

             usuario.UserName = txtUsuario.Text;
             usuario.Password = txtContraseña.Password;


             usuario = TrabajarUsuarios.buscarUsuario(usuario.UserName, usuario.Password);

             
             // Usuario userEnviar = new Usuario();
             //userEnviar = TrabajarUsuarios.traerUnUsuario(usuario.UserName);
            

            if(usuario !=null && usuario.Rol == "Operador"){

                MessageBox.Show("Bienvenido Operador");

                //PantallaPrincipal.usuarioLogin.Rol = "Operador";

                PantallaPrincipal pp = new PantallaPrincipal();
                pp.usuarioLogin = usuario;
                pp.Show();
                this.Hide();
            }
            else if (usuario != null && usuario.Rol == "Administrador")
            {

                MessageBox.Show("Bienvenido Administrador" );
                //PantallaPrincipal.usuarioLogin.Rol = "Administrador";
                PantallaPrincipal pp = new PantallaPrincipal();
                pp.usuarioLogin = usuario;
                pp.Show();
                this.Hide();
            }
            else {
                MessageBox.Show("El Usuario ingresado no existe!", "Error de Acceso", MessageBoxButton.OK, MessageBoxImage.Error);
            } 

        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Presentacion pr = new Presentacion();
            pr.Show();
           
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

     
 
      
        

      
    }
}
