﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;
namespace Vistas
{

    public class ConversorDeEstados:IValueConverter
        
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            SolidColorBrush rojoclaro = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#F5A9A9"));
            SolidColorBrush rojoOscuro = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#DF0101"));
            if (value != null)
            {
                string cadena = (string)value;
                int valor = Int32.Parse(cadena);

                if (valor == 0) {

                    return Brushes.Green;
                }
                else if (valor >0 && valor<=30){

                    return rojoclaro;
                }
                else if (valor >30 && valor<=60) {

                    return Brushes.Red;
                }
                else if (valor > 60) {
                    return rojoOscuro;

                }

            }




            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
