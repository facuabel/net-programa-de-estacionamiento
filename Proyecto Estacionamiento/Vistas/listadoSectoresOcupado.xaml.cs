﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using ClasesBase;




namespace Vistas
{
    /// <summary>
    /// Lógica de interacción para listadoSectoresOcupado.xaml
    /// </summary>
    public partial class listadoSectoresOcupado : Window
    {
        public listadoSectoresOcupado()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DataTable dt = new DataTable();
            dt = TrabajarSectores.TraerSector();
            //var mylist = dt.AsEnumerable().ToList();
            listView1.ItemsSource = dt.DefaultView;
            

        }
    }
}
