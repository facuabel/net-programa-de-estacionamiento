﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Globalization;
using System.Data;
using ClasesBase;
using System.Collections.ObjectModel;
namespace Vistas
{
    /// <summary>
    /// Lógica de interacción para Listado_de_Usuarios.xaml
    /// </summary>
    public partial class Listado_de_Usuarios : Window
    {
       // CollectionViewSource cv;
        public Listado_de_Usuarios()
        {
            InitializeComponent();
            //cv = (CollectionViewSource)this.Resources["MisUsuariosOrdenados"];
            
            CollectionView vista = (CollectionView)CollectionViewSource.GetDefaultView(listView1.ItemsSource);
            vista.Filter = UserFilter;
        }

        ObservableCollection<Usuario> miLista;

        private bool UserFilter(object item)
		{
			if(String.IsNullOrEmpty(txtFiltro.Text))
				return true;
			else
				return ((item as Usuario).UserName.IndexOf(txtFiltro.Text, StringComparison.OrdinalIgnoreCase) >= 0);
		}


        private void txtFiltro_TextChanged(object sender, TextChangedEventArgs e)
        {
        
        }

        //private void CollectionViewSource_Filter(object sender, FilterEventArgs e) {

        //    //string cadena = txtFiltrar.Text;
        //    Usuario oUsuario = e.Item as Usuario;

        //    if (oUsuario != null)
        //    {
        //        if (oUsuario.UserName.Contains(cadena))
        //        {
        //            e.Accepted = true;

        //        }
        //        else {
        //            e.Accepted = false;
        //        }
        //    }
        

        private void button1_Click(object sender, RoutedEventArgs e)
        {


            CollectionViewSource.GetDefaultView(listView1.ItemsSource).Refresh();
            
            //if (cv != null) 
            //{
            //    cv.Filter += new FilterEventHandler(CollectionViewSource_Filter);
            //}
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
           miLista = ((ObjectDataProvider)this.Resources["MisUsuarios"]).Data as ObservableCollection<Usuario>;
            
        }

        private void btnImprimir_Click(object sender, RoutedEventArgs e)
        {
             ItemCollection items = listView1.Items;

            vistaPreviaUsuarios vistaImpresion = new vistaPreviaUsuarios(items);            
            vistaImpresion.Show();
        }

        
    }
}
