﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClasesBase;

namespace Vistas
{
    /// <summary>
    /// Lógica de interacción para ValidarCliente.xaml
    /// </summary>
    public partial class ValidarCliente : Window
    {
        public ValidarCliente()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Cliente uCliente = new Cliente();
            TrabajarClientes trabajarCliente = new TrabajarClientes();

            uCliente.ClienteDNI = System.Convert.ToInt32(txtDni.Text);
            uCliente.Apellido = txtApellido.Text;
            uCliente.Nombre = txtNombre.Text;
            uCliente.Telefono = txtTelefono.Text;

            try
            {
                trabajarCliente.AgregarCliente(uCliente);
                MessageBox.Show("Cliente Creado Correctamente");
            }
            catch { MessageBox.Show("Verifique que los Datos esten correctamente puesto"); }

        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Cliente uCliente = new Cliente();
            TrabajarClientes trabajarCliente = new TrabajarClientes();

            uCliente.ClienteDNI = System.Convert.ToInt32(txtDni.Text);
            uCliente.Apellido = txtApellido.Text;
            uCliente.Nombre = txtNombre.Text;
            uCliente.Telefono = txtTelefono.Text;


            trabajarCliente.modificarCliente(uCliente);

            MessageBox.Show("Cliente modificado");
        

           
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {

            Cliente uCliente = new Cliente();
            TrabajarClientes trabajarCliente = new TrabajarClientes();

            uCliente.ClienteDNI = System.Convert.ToInt32(txtDni.Text);
            
            trabajarCliente.eliminarCliente(uCliente);

            MessageBox.Show("Cliente eliminado ");


        }


       

    }
}
