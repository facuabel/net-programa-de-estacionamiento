﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClasesBase;
namespace Vistas
{
    /// <summary>
    /// Lógica de interacción para GestionDeEstacionamiento.xaml
    /// </summary>
    public partial class GestionDeEstacionamiento : Window
    {

        public Usuario usuarioGE = new Usuario();

        public GestionDeEstacionamiento()
        {
            InitializeComponent();
        }

        private void btnZonaNorte_Click(object sender, RoutedEventArgs e)
        {
            ZonaNorte zona = new ZonaNorte();
            zona.UsuarioZN = usuarioGE;
            zona.Show();

        }

        private void btnZonaSur_Click(object sender, RoutedEventArgs e)
        {
            ZonaSur zona = new ZonaSur();
            zona.usuarioZS = usuarioGE;
            zona.Show();
        }

        private void btnZonaEste_Click(object sender, RoutedEventArgs e)
        {
            ZonaEste zona = new ZonaEste();
            zona.usuarioZE = usuarioGE;
            zona.Show();
        }

       
        

       
       

       
       

        

    }
}
