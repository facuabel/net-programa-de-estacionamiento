﻿using System;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Data;
using System.Windows.Threading;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Documents;

namespace Vistas
{
    /// <summary>
    /// Lógica de interacción para AcercaDe.xaml
    /// </summary>
    public partial class AcercaDe : Window
    {

        private bool mediaPlayerIsPlaying = false;
        private bool userIsDraggingSlider = false;

        public AcercaDe()
        {
            InitializeComponent();
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
            timer.Start();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if ((reproductor.Source != null) && (reproductor.NaturalDuration.HasTimeSpan) && (!userIsDraggingSlider))
            {
                barraRepro.Minimum = 0;
                barraRepro.Maximum = reproductor.NaturalDuration.TimeSpan.TotalSeconds;
                barraRepro.Value = reproductor.Position.TotalSeconds;
            }
        }


        private void Play_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = (reproductor != null) && (reproductor.Source != null);
        }


        private void Play_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            reproductor.Play();
            mediaPlayerIsPlaying = true;
        }

        private void Pause_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = mediaPlayerIsPlaying;
        }

        private void Pause_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            reproductor.Pause();
        }

        private void Stop_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = mediaPlayerIsPlaying;
        }

        private void Stop_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            reproductor.Stop();
            mediaPlayerIsPlaying = false;
        }



        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            reproductor.Source = new Uri(@"D:\disco local d\apu\laboratorio programacion orientada a objeto 2\practica\tpUltimaVersion\Vistas\Video_Musica\beret.mp4",
                                     UriKind.Relative);

            //barraRepro.Maximum = reproductor.NaturalDuration.TimeSpan.TotalSeconds;
        }




        //private void Grid_MouseWheel(object sender, MouseWheelEventArgs e)
        //{
        //    reproductor.Volume += (e.Delta > 0) ? 0.1 : -0.1;
        //}

        private void ChangeMediaVolume(object sender, RoutedPropertyChangedEventArgs<double> args)
        {

            reproductor.Volume = (double)volumen.Value;
        }

        private void barraRepro_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            userIsDraggingSlider = false;
            reproductor.Position = TimeSpan.FromSeconds(barraRepro.Value);
        }

        private void barraRepro_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            reproductor.Position = TimeSpan.FromSeconds(barraRepro.Value);
            estadoRepro.Text = TimeSpan.FromSeconds(barraRepro.Value).ToString(@"hh\:mm\:ss");

        }


        private void barraRepro_DragStarted(object sender, DragStartedEventArgs e)
        {
            userIsDraggingSlider = true;
        }

    }
}
