﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Reflection;

namespace Vistas
{
    /// <summary>
    /// Lógica de interacción para Presentacion.xaml
    /// </summary>
    public partial class Presentacion : Window
    {
        public Presentacion()
        {
            InitializeComponent();
            DispatcherTimer disp = new DispatcherTimer();
            disp.Interval = new TimeSpan(0, 0, 1);
            disp.Tick += (s, a) =>
                {
                    rectangle1.Fill = PickBrush();
                    rectangle13.Fill = PickBrush();
                    rectangle14.Fill = PickBrush();
                    rectangle15.Fill = PickBrush();
                    rectangle16.Fill = PickBrush();
                    rectangle17.Fill = PickBrush();
                    rectangle18.Fill = PickBrush();
                    rectangle2.Fill = PickBrush();
                    rectangle3.Fill = PickBrush();
                    rectangle4.Fill = PickBrush();
                    rectangle5.Fill = PickBrush();
                    rectangle6.Fill = PickBrush();
                    label1.Foreground = PickBrush();
                    label2.Foreground = PickBrush();
                };

           
            disp.Start();





        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            reproductor.Source = new Uri(@"D:\disco local d\apu\laboratorio programacion orientada a objeto 2\practica\tpUltimaVersion\Vistas\Video_Musica\myHouse.mp3",
                                   UriKind.Relative);
            reproductor.Play();
        }



        private Brush PickBrush()
        {
            Brush result = Brushes.Transparent;

            Random rnd = new Random();

            Type brushesType = typeof(Brushes);

            PropertyInfo[] properties = brushesType.GetProperties();

            int random = rnd.Next(properties.Length);
            result = (Brush)properties[random].GetValue(null, null);

            return result;
        }
    
    }
}
