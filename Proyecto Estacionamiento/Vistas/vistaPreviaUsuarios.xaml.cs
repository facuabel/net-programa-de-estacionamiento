﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClasesBase;


namespace Vistas
{
    /// <summary>
    /// Lógica de interacción para vistaPreviaUsuarios.xaml
    /// </summary>
    public partial class vistaPreviaUsuarios : Window
    {
        private ItemCollection itemsUsuarios;

       

        //private System.Collections.ObjectModel.ObservableCollection<Usuario> listaUsuarios;
        
        public vistaPreviaUsuarios()
        {
            InitializeComponent();
        }

        public vistaPreviaUsuarios(ItemCollection items)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            this.itemsUsuarios = items;
            listUsuarios.DataContext = items;
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            PrintDialog printDialog = new PrintDialog();
            if (printDialog.ShowDialog() == true)
            {
                printDialog.PrintDocument(((IDocumentPaginatorSource)documentoAImprimir).DocumentPaginator, "Imprimir");
                this.Close();
            }
        }

        
       
    }
}
