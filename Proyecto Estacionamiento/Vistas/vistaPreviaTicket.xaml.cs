﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClasesBase;

namespace Vistas
{
    /// <summary>
    /// Lógica de interacción para vistaPreviaTicket.xaml
    /// </summary>
    public partial class vistaPreviaTicket : Window
    {
        public Usuario userVS = new Usuario();

        int dni;

        public int Dni
        {
            get { return dni; }
            set { dni = value; }
        }
        Double tarifa;
        
        public Double Tarifa
        {
            get { return tarifa; }
            set { tarifa = value; }
        }
        string patente;

        public string Patente
        {
            get { return patente; }
            set { patente = value; }
        }
        string ingreso;

        public string Ingreso
        {
            get { return ingreso; }
            set { ingreso = value; }
        }
        string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        int sector;

        public int Sector
        {
            get { return sector; }
            set { sector = value; }
        }

        
        string tipoVehiculo;

        public string TipoVehiculo
        {
            get { return tipoVehiculo; }
            set { tipoVehiculo = value; }
        }

        int nroTicket;

        public int NroTicket
        {
            get { return nroTicket; }
            set { nroTicket = value; }
        }


        public vistaPreviaTicket()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            labelFecha.Content = DateTime.Now;
           
            txtNombreCliente.Text = nombre;
            txtPatente.Text = patente;
            txtTipoVehiculo.Text = tipoVehiculo;
            txtTarifa.Text = tarifa.ToString();
            txtSector.Text = sector.ToString();
            labelNroticket.Content = nroTicket;
        

         
            
        }


       

        

    }
}
