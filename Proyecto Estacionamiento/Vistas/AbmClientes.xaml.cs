﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClasesBase;

namespace Vistas
{
    /// <summary>
    /// Lógica de interacción para AbmClientes.xaml
    /// </summary>
    public partial class AbmClientes : Window
    {
        public AbmClientes()
        {
            InitializeComponent();
        }

        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {


            try
            {
                Cliente Ocliente = new Cliente();
                Ocliente.ClienteDNI = System.Convert.ToInt32(txtDni.Text);
                Ocliente.Apellido = txtApellido.Text;
                Ocliente.Nombre = txtNombre.Text;
                Ocliente.Telefono = txtTelefono.Text;

                if (MessageBox.Show("Seguro que desea Guardar los datos \n" +
                       "Numero de DNI :" + Ocliente.ClienteDNI +
                       "\n Nombre : " + Ocliente.Nombre +
                       "\n Apellido : " + Ocliente.Apellido +
                       "\n Telefono : " + Ocliente.Telefono, "Confirmacion", MessageBoxButton.YesNo) == (System.Windows.MessageBoxResult.Yes))
                {
                    MessageBox.Show("se guardaron los datos");
                }
                else
                {
                    MessageBox.Show("no se guardaron los datos");
                }

            }
            catch { MessageBox.Show("Verifique que los datos esten completados", "Verificacion", MessageBoxButton.OK, MessageBoxImage.Information); }
            

        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            //Int32 dniBuscar = System.Convert.ToInt32(txtDni.Text);
            //Cliente cliente1 = TrabajarClientes.TraerCliente(dniBuscar);

            //txtNombre.Text = cliente1.Nombre;
            //txtApellido.Text = cliente1.Apellido;
            //txtTelefono.Text = cliente1.Telefono;

        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            ValidarCliente vc = new ValidarCliente();
            vc.Show();
        }





    }
}
