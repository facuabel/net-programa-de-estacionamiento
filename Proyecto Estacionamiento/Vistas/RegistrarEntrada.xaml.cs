﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClasesBase;

namespace Vistas
{
    /// <summary>
    /// Lógica de interacción para RegistrarEntrada.xaml
    /// </summary>
    public partial class RegistrarEntrada : Window
    {
        public  Usuario usuarioRE = new Usuario();

        private int ZonaNro;

        public int ZonaNro1
        {
            get { return ZonaNro; }
            set { ZonaNro = value; }
        }


        private int sectorCodigo;

        public int SectorCodigo
        {
            get { return sectorCodigo; }
            set { sectorCodigo = value; }
        }
        
        


        public RegistrarEntrada()
        {
            InitializeComponent();
           // cmbClientes.SelectedValuePath = "nombreC";
          
           // tiempo();
           //// carga();
           
         
        }

       
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            labelFechaHoraIngreso.Content = DateTime.Now.ToString() ;
            txtSector.Text = sectorCodigo.ToString();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Cliente cliente = new Cliente();
            TipoVehiculo tipoV = new TipoVehiculo();

            string fechaHora = labelFechaHoraIngreso.Content.ToString();
            int clienteDNI = Convert.ToInt32(cmbClientes.SelectedValue.ToString());
            int codigo = Convert.ToInt32(cmbVehiculos.SelectedValue.ToString());
            int sec = Convert.ToInt32(txtSector.Text);
            string patente = txtPatente.Text;
           

            //obtengo un objeto cliente
            cliente = TrabajarClientes.TraerUNCliente(clienteDNI);


            //Obtengo un objeto tipoVehiculo para la tarifa
            tipoV = TrabajarTipoVehiculos.TraerUNTipoVehiculo(codigo);

            int nroTicket = TrabajarTicket.cantidadTotalTickets() + 1;

            vistaPreviaTicket vs = new vistaPreviaTicket();

            vs.Patente= patente;
            vs.Nombre = cliente.Apellido;
            vs.Sector = sec;
            vs.TipoVehiculo = tipoV.Descripcion;
            vs.Tarifa = tipoV.Tarifa;
            vs.NroTicket = nroTicket;
           
            //Insert            
            Ticket ticket = new Ticket();
            ticket.ClienteDNI = clienteDNI;
            ticket.FechaHoraEnt = DateTime.Now;
            ticket.Patente = patente;
            //tendriamos que sacar el codigo de sector de una tabla
            ticket.SectorCodigo = sec;
            ticket.Tarifa = tipoV.Tarifa;
            ticket.TVCodigo1 = tipoV.TVCodigo1;
            ticket.UserName1 = usuarioRE.UserName;


            //Le cambiamos el estado a sector
            string estado = "false";
            TrabajarSectores.cambiarEstado(sectorCodigo, ZonaNro1, estado);
           TrabajarTicket.agregarTicket(ticket);
           string user = TrabajarTicket.traerNombreUsuario(Convert.ToInt32(txtSector.Text));
           vs.txtNombreUsuario.Text = user;  
            vs.Show();


           

        }





        //private void tiempo() {

        //    txtTiempoActual.Content = DateTime.Now.ToString();
        //}

        //private void carga()
        //{
        //    TrabajarEstacionamiento tE = new TrabajarEstacionamiento();
        //    List<Cliente> clienteList = new List<Cliente>();

        //    clienteList.Add(tE.TraerCliente());
        //    this.cmbCliente.DisplayMemberPath = "Apellido";
        //    this.cmbCliente.ItemsSource = null;
        //    this.cmbCliente.ItemsSource = clienteList;


        //}








        
    }
}
