﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClasesBase
{
    public class TipoVehiculo
    {
        private int TVCodigo;

        public int TVCodigo1
        {
            get { return TVCodigo; }
            set { TVCodigo = value; }
        }
        private string descripcion;

        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }
        private int tarifa;

        public int Tarifa
        {
            get { return tarifa; }
            set { tarifa = value; }
        }

       

        private string imagen;

        public string Imagen
        {
            get { return imagen; }
            set { imagen = value; }
        }
    }
}
