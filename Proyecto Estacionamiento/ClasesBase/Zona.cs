﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClasesBase
{
   public class Zona
    {

        private int zonaCodigo;

        public int ZonaCodigo
        {
            get { return zonaCodigo; }
            set { zonaCodigo = value; }
        }
        private string descripcion;

        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }
        private string piso;

        public string Piso
        {
            get { return piso; }
            set { piso = value; }
        }
    }
}
