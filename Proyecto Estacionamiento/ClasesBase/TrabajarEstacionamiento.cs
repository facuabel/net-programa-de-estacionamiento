﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace ClasesBase
{
   public class TrabajarEstacionamiento
    {
       public Cliente TraerCliente()
       {
           
           //conectar la conexion
           SqlConnection cnn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
           //2 paso crear el comando
           SqlCommand cmd = new SqlCommand();
           //3segundo configurar el 
           cmd.CommandText = @"Select * from Clientes";
           cmd.CommandType = CommandType.Text;
           cmd.Connection = cnn;
          

           Cliente oCliente = null;

           cnn.Open();
           SqlDataReader reader = cmd.ExecuteReader();

           while (reader.Read() == true)
           {
               oCliente = new Cliente();
               oCliente.ClienteDNI = (Int32)reader["ClienteDNI"];
               oCliente.Apellido = (string)reader["Apellido"];
               oCliente.Nombre = (string)reader["Nombre"];
               oCliente.Telefono = (string)reader["Telefono"];
               
           }
           cnn.Close();

           
           return oCliente;
       }


       public DataTable TraerCliente2()
            {
                using (DataTable dt = new DataTable())
                {
                    using (SqlConnection cn = new
                        SqlConnection(ClasesBase.Properties.Settings.Default.conexion))
                    {
                    using (SqlDataAdapter da = new SqlDataAdapter("Select (Apellido + ', ' + Nombre) as nombreC, ClienteDNI from Clientes", cn))
                            {   
                                da.Fill(dt);
                            }
                        }
                    return dt;
                            }
                    }




       public DataTable TraerVehiculo()
            {
                using (DataTable dt = new DataTable())
                {
                    using (SqlConnection cn = new
                        SqlConnection(ClasesBase.Properties.Settings.Default.conexion))
                    {
                    using (SqlDataAdapter da = new SqlDataAdapter("Select * From TipoVehiculo", cn))
                            {   
                                da.Fill(dt);
                            }
                        }
                    return dt;
                            }
                    }
                }
    }




