﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace ClasesBase
{
   public class TrabajarClientes
    {
       public Cliente TraerCliente(string dni)
       {

           //conectar la conexion
           SqlConnection cnn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
           //2 paso crear el comando
           SqlCommand cmd = new SqlCommand();
           //3segundo configurar el 
           cmd.CommandText = @"select * from Clientes
                                where ClienteDNI = @dni";
           cmd.CommandType = CommandType.Text;
           cmd.Connection = cnn;
           cmd.Parameters.AddWithValue("@dni", dni);

           Cliente oCliente = null;

           cnn.Open();
           SqlDataReader reader = cmd.ExecuteReader();

           while (reader.Read() == true)
           {
               oCliente = new Cliente();
               oCliente.ClienteDNI = (Int32)reader["ClienteDNI"];
               oCliente.Apellido = (string)reader["Apellido"];
               oCliente.Nombre = (string)reader["Nombre"];
               oCliente.Telefono = (string)reader["Telefono"];
           }
           cnn.Close();


           return oCliente;
       }

       public void AgregarCliente(Cliente oCliente) { 
        //conectar la conexion
           SqlConnection cnn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
           //2 paso crear el comando
           SqlCommand cmd = new SqlCommand();
           //3segundo configurar el 
           cmd.CommandText = "InsertarCliente";
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Connection = cnn;
           cmd.Parameters.AddWithValue("@Ape", oCliente.Apellido);
           cmd.Parameters.AddWithValue("@DNI", oCliente.ClienteDNI);
           cmd.Parameters.AddWithValue("@Nom", oCliente.Nombre);
           cmd.Parameters.AddWithValue("@Tel", oCliente.Telefono);
           //4to paso ejecutar la intrsiccion SQL
           cnn.Open();
           cmd.ExecuteNonQuery();
           cnn.Close();
       }

       public void modificarCliente(Cliente oCliente)
       {
           //conectar la conexion
           SqlConnection cnn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
           //2 paso crear el comando
           SqlCommand cmd = new SqlCommand();
           //3segundo configurar el 
           cmd.CommandText = "modificarCliente";
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Connection = cnn;
           cmd.Parameters.AddWithValue("@Ape", oCliente.Apellido);
           cmd.Parameters.AddWithValue("@DNI", oCliente.ClienteDNI);
           cmd.Parameters.AddWithValue("@Nom", oCliente.Nombre);
           cmd.Parameters.AddWithValue("@Tel", oCliente.Telefono);
           //4to paso ejecutar la intrsiccion SQL
           cnn.Open();
           cmd.ExecuteNonQuery();
           cnn.Close();
       }


      

       public void eliminarCliente(Cliente oCliente)
       {
           //conectar la conexion
           SqlConnection cnn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
           //2 paso crear el comando
           SqlCommand cmd = new SqlCommand();
           //3segundo configurar el 
           cmd.CommandText = "eliminarCliente";
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Connection = cnn;
           cmd.Parameters.AddWithValue("@DNI", oCliente.ClienteDNI);
           //4to paso ejecutar la intrsiccion SQL
           cnn.Open();
           cmd.ExecuteNonQuery();
           cnn.Close();
       }


       public static Cliente TraerUNCliente(int dni)
       {

           //conectar la conexion
           SqlConnection cnn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
           //2 paso crear el comando
           SqlCommand cmd = new SqlCommand();
           //3segundo configurar el 
           cmd.CommandText = @"select * from Clientes
                                where ClienteDNI = @dni";
           cmd.CommandType = CommandType.Text;
           cmd.Connection = cnn;
           cmd.Parameters.AddWithValue("@dni", dni);

           Cliente oCliente = null;

           cnn.Open();
           SqlDataReader reader = cmd.ExecuteReader();

           while (reader.Read() == true)
           {
               oCliente = new Cliente();
               oCliente.ClienteDNI = (Int32)reader["ClienteDNI"];
               oCliente.Apellido = (string)reader["Apellido"];
               oCliente.Nombre = (string)reader["Nombre"];

               string nom = oCliente.Apellido;
               string ape = oCliente.Nombre;

               oCliente.Apellido = nom + ','+ ape;
               oCliente.Telefono = (string)reader["Telefono"];
           }
           cnn.Close();
            

           return oCliente;
       }
    }
}
