﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace ClasesBase
{
   public class TrabajarTicket
    {

       public static int cantidadTotalTickets()
       {

           //conectar la conexion
           SqlConnection cnn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
           //2 paso crear el comando
           SqlCommand cmd = new SqlCommand();
           //3segundo configurar el comando

           cmd.CommandText = "ConsultaCantidad";
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Connection = cnn;
           //4to paso ejecutar la consulta
           //parametros de salida
           cmd.Parameters.Add("@cantidad", SqlDbType.Int);
           cmd.Parameters["@cantidad"].Direction = ParameterDirection.Output;

           cnn.Open();
           cmd.ExecuteNonQuery();
           cnn.Close();

           int cantidad = (int)cmd.Parameters["@cantidad"].Value;

           return cantidad;

       }


       public static DateTime fechaHoraSector(int sector)
       {

           //conectar la conexion
           SqlConnection cnn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
           //2 paso crear el comando
           SqlCommand cmd = new SqlCommand();
           //3segundo configurar el comando

           cmd.CommandText = "fechaHoraSector";
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Connection = cnn;
           //4to paso ejecutar la consulta
           cmd.Parameters.AddWithValue("@sector", sector);
           //parametros de salida
           //cmd.Parameters.Add("@fechaHora", SqlDbType.DateTime);

           //cmd.Parameters["@fechaHora"].Direction = ParameterDirection.Output;
           DateTime fechaHora;
           cnn.Open();
           try
           {
                fechaHora = (DateTime)cmd.ExecuteScalar();
           }
           catch (Exception)
           {

                fechaHora = new DateTime(2018, 11, 1);
           }
           
           cnn.Close();

           //DateTime fechaHora = (DateTime)cmd.Parameters["@fechaHora"].Value;

           return fechaHora;

       }


       public static DateTime fechaHoraSector2(int sector,int zona)
       {

           //conectar la conexion
           SqlConnection cnn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
           //2 paso crear el comando
           SqlCommand cmd = new SqlCommand();
           //3segundo configurar el comando

           cmd.CommandText = "m";
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Connection = cnn;
           //4to paso ejecutar la consulta
           cmd.Parameters.AddWithValue("@sector", sector);
           cmd.Parameters.AddWithValue("@zona", zona);
           //parametros de salida
           //cmd.Parameters.Add("@fechaHora", SqlDbType.DateTime);

           //cmd.Parameters["@fechaHora"].Direction = ParameterDirection.Output;
           DateTime fechaHora;
           cnn.Open();
           try
           {
               fechaHora = (DateTime)cmd.ExecuteScalar();
           }
           catch (Exception)
           {

               fechaHora = new DateTime(2018, 11, 1);
           }

           cnn.Close();

           //DateTime fechaHora = (DateTime)cmd.Parameters["@fechaHora"].Value;

           return fechaHora;

       }


       public static void agregarTicket(Ticket oTicket)
       {

           //conectar la conexion
           SqlConnection cnn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
           //2 paso crear el comando
           SqlCommand cmd = new SqlCommand();
           //3segundo configurar el comando

           cmd.CommandText = "agregarTicket";
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Connection = cnn;
           cmd.Parameters.AddWithValue("@dniC", oTicket.ClienteDNI);
           //cmd.Parameters.AddWithValue("@dur", 2);
           cmd.Parameters.AddWithValue("@fechaEnt", oTicket.FechaHoraEnt);
          // cmd.Parameters.AddWithValue("@fechaSal", null);
           cmd.Parameters.AddWithValue("@pate", oTicket.Patente);
           cmd.Parameters.AddWithValue("@sector", oTicket.SectorCodigo);
           cmd.Parameters.AddWithValue("@tarifa", oTicket.Tarifa);
           //cmd.Parameters.AddWithValue("@total" , null);
           cmd.Parameters.AddWithValue("@tvCodigo", oTicket.TVCodigo1);
           cmd.Parameters.AddWithValue("@UserName", oTicket.UserName1);



           //4to paso ejecutar la intrsiccion SQL
           cnn.Open();
           cmd.ExecuteNonQuery();
           cnn.Close();

       }



       public static DateTime fechaHoraEntrada(int sector)
       {

           //conectar la conexion
           SqlConnection cnn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
           //2 paso crear el comando
           SqlCommand cmd = new SqlCommand();
           //3segundo configurar el comando

           cmd.CommandText = "ultimaFechaEntrada";
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Connection = cnn;
           //4to paso ejecutar la consulta
           cmd.Parameters.AddWithValue("@sector", sector);
           //parametros de salida
           cmd.Parameters.Add("@fechaHora", SqlDbType.DateTime);

           cmd.Parameters["@fechaHora"].Direction = ParameterDirection.Output;
           
           cnn.Open();
           cmd.ExecuteNonQuery();
           cnn.Close();

           DateTime fechaHora = (DateTime)cmd.Parameters["@fechaHora"].Value;

           return fechaHora;

       }


       public static TipoVehiculo traerTipoVehiculo(int codigo)
       {
           //conectar la conexion
           SqlConnection cnn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
           //2 paso crear el comando
           SqlCommand cmd = new SqlCommand();
           //3segundo configurar el 
           cmd.CommandText = @"select * from vistaVehiculo where sectorCodigo = @sector and ticketNro=(Select Max(ticketNro) from Ticket where sectorCodigo=@sector)";
           cmd.CommandType = CommandType.Text;
           cmd.Connection = cnn;
           cmd.Parameters.AddWithValue("@sector", codigo);

           TipoVehiculo tipoV = null;

           cnn.Open();
           SqlDataReader reader = cmd.ExecuteReader();

           while (reader.Read() == true)
           {
               tipoV = new TipoVehiculo();
               tipoV.Descripcion = (string)reader["descripcion"];
               tipoV.Tarifa = (int)reader["tarifa"];
           }
           cnn.Close();


           return tipoV;
       }

       public static string traerNombreCompleto(int codigoSector)
       {
           //conectar la conexion
           SqlConnection cnn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
           //2 paso crear el comando
           SqlCommand cmd = new SqlCommand();
           //3segundo configurar el 
           cmd.CommandText = @"select nombreCompleto from vistaSalida where sectorCodigo = @sector and ticketNro=(Select Max(ticketNro) from Ticket where sectorCodigo=@sector)";
           cmd.CommandType = CommandType.Text;
           cmd.Connection = cnn;
           cmd.Parameters.AddWithValue("@sector", codigoSector);

           string nombre = null;

           cnn.Open();
           SqlDataReader reader = cmd.ExecuteReader();

           while (reader.Read() == true)
           {
               
               nombre = (string)reader["nombreCompleto"];
              
           }
           cnn.Close();


           return nombre; 
       }

       public static string traerPatente(int codigoSector)
       {
           //conectar la conexion
           SqlConnection cnn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
           //2 paso crear el comando
           SqlCommand cmd = new SqlCommand();
           //3segundo configurar el 
           cmd.CommandText = @"select patente from vistaSalida where sectorCodigo = @sector and ticketNro=(Select Max(ticketNro) from Ticket where sectorCodigo=@sector)";
           cmd.CommandType = CommandType.Text;
           cmd.Connection = cnn;
           cmd.Parameters.AddWithValue("@sector", codigoSector);

           string patente = null;

           cnn.Open();
           SqlDataReader reader = cmd.ExecuteReader();

           while (reader.Read() == true)
           {

               patente = (string)reader["patente"];

           }
           cnn.Close();


           return patente;
       }

       public static int traerNroTicket(int codigoSector)
       {
           //conectar la conexion
           SqlConnection cnn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
           //2 paso crear el comando
           SqlCommand cmd = new SqlCommand();
           //3segundo configurar el 
           cmd.CommandText = @"select ticketNro from vistaSalida where sectorCodigo = @sector and ticketNro=(Select Max(ticketNro) from Ticket where sectorCodigo=@sector)";
           cmd.CommandType = CommandType.Text;
           cmd.Connection = cnn;
           cmd.Parameters.AddWithValue("@sector", codigoSector);

           int NroTicket = 0;

           cnn.Open();
           SqlDataReader reader = cmd.ExecuteReader();

           while (reader.Read() == true)
           {

               NroTicket = (int)reader["ticketNro"];

           }
           cnn.Close();


           return NroTicket;
       }

       public static void actualizarTicket(Ticket ticketUpdate)
       {
           //conectar la conexion
           SqlConnection cnn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
           //2 paso crear el comando
           SqlCommand cmd = new SqlCommand();
           //3segundo configurar el 
           cmd.CommandText = "modificarTicket";
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Connection = cnn;
           cmd.Parameters.AddWithValue("@nro", ticketUpdate.TicketNro);
           cmd.Parameters.AddWithValue("@dur", ticketUpdate.Duracion);
           cmd.Parameters.AddWithValue("@fechaSal", ticketUpdate.FechaHoraSal);
           cmd.Parameters.AddWithValue("@total", ticketUpdate.Total);
           //4to paso ejecutar la intrsiccion SQL
           cnn.Open();
           cmd.ExecuteNonQuery();
           cnn.Close();
       }




       public static string traerNombreUsuario(int codigoSector)
       {
           //conectar la conexion
           SqlConnection cnn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
           //2 paso crear el comando
           SqlCommand cmd = new SqlCommand();
           //3segundo configurar el 
           cmd.CommandText = @"select usuarioNombre from vistaSalida where sectorCodigo = @sector and ticketNro=(Select Max(ticketNro) from Ticket where sectorCodigo=@sector)";
           cmd.CommandType = CommandType.Text;
           cmd.Connection = cnn;
           cmd.Parameters.AddWithValue("@sector", codigoSector);

           string nombre = null;

           cnn.Open();
           SqlDataReader reader = cmd.ExecuteReader();

           while (reader.Read() == true)
           {

               nombre = (string)reader["usuarioNombre"];

           }
           cnn.Close();


           return nombre;
       }



       public static DataTable TraerVentas(string FechaDesde,string FechaHasta)
       {

                DataTable  dt = new DataTable ();

                using (SqlConnection cnn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion))
                {

                    string query = @"select *  from ListadoVentas  where fechaHoraEnt >= @DE and FechaHoraSal<@DF ";
                    //  string query = @"UPDATE <tabla> SET campo1 = @param1, campo2 = @param2 WHERE id = @id";

                    SqlCommand cmd = new SqlCommand(query, cnn);
                    cmd.Parameters.AddWithValue("DE", FechaDesde);
                    cmd.Parameters.AddWithValue("DF", FechaHasta);

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                }

                return dt;
       }
    }







}
