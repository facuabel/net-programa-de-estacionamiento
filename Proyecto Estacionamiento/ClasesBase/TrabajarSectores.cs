﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;


namespace ClasesBase
{
   public class TrabajarSectores
    {

       public static DataTable TraerSector()
       {
           using (DataTable dt = new DataTable())
           {
               using (SqlConnection cn = new
                   SqlConnection(ClasesBase.Properties.Settings.Default.conexion))
               {
                   using (SqlDataAdapter da = new SqlDataAdapter("select * from vistaListadoSectoresO  ", cn))
                   {
                       da.Fill(dt);
                   }
               }
               return dt;
           }
       }


       public static void cambiarEstado(int sectorCodigo, int ZonaNro1, string estado)
       {
           //conectar la conexion
           SqlConnection cnn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
           //2 paso crear el comando
           SqlCommand cmd = new SqlCommand();
           //3segundo configurar el 
           cmd.CommandText = "modificarSector";
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Connection = cnn;
           cmd.Parameters.AddWithValue("@nroZona", ZonaNro1);
           cmd.Parameters.AddWithValue("@sector", sectorCodigo);
           cmd.Parameters.AddWithValue("@estado", estado);
           //4to paso ejecutar la intrsiccion SQL
           cnn.Open();
           cmd.ExecuteNonQuery();
           cnn.Close();
       
       }
    }
}
