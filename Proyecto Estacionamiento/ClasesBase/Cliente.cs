﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//para la interface IdataErrorInfo
using System.ComponentModel;

namespace ClasesBase
{
    public class Cliente:IDataErrorInfo
    {
        private int clienteDNI;

        public int ClienteDNI
        {
            get { return clienteDNI; }
            set { clienteDNI = value; }
        }
        private string apellido;

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        private string telefono;

        public string Telefono
        {
            get { return telefono; }
            set { telefono = value; }
        }

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {

            get
            {
                string result = string.Empty;
                if (columnName == "ClienteDNI")
                {
                    if (ClienteDNI < 1000000)
                    {

                        result = "El Dni debe ser mayor a 1.000.000";

                    }
                }
                else if (columnName == "Apellido")
                {
                    if (string.IsNullOrEmpty(Apellido))
                        result = "Debe Ingresar el Apellido";
                    else if (Apellido.Length < 3)
                        result = "El Apellido debe tener al menos 3 caracteres";
                }
                else if (columnName == "Nombre")
                {
                    if (string.IsNullOrEmpty(Nombre))
                        result = "Debe Ingresar el Nombre";
                    else if (Nombre.Length < 3)
                        result = "El Nombre debe tener al menos 3 caracteres";
                }
                else if (columnName == "Telefono")
                {
                    if (string.IsNullOrEmpty(Telefono))
                        result = "Debe Ingresar el Telefono";
                    else if (Telefono.Length < 3)
                        result = "El Telefono debe tener al menos 6 caracteres numerico";
                }

                return result;
            }
        }
    }
}
