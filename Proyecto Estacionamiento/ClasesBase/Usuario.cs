﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Globalization;


namespace ClasesBase
{
    public class Usuario:INotifyPropertyChanged
    {
       

        private string userName;

        public string UserName
        {
            get { return userName; }
            set
            {
                userName = value;
                Notificador("userName");
            }
        }
        private string password;

        public string Password
        {
            get { return password; }
            set { password = value;
            Notificador("password");}
        }
        private string apellido;

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value;
            Notificador("apellido");
            }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value;
            Notificador("nombre");
            }
        }
        private string rol;

        public string Rol
        {
            get { return rol; }
            set { rol = value;
                Notificador("rol"); }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public void Notificador(string prop){
            if (PropertyChanged != null){
            PropertyChanged(this, new PropertyChangedEventArgs(prop));
            }
        
        }

        
    }
}
