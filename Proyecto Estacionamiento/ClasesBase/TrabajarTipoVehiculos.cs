﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace ClasesBase
{
    public class TrabajarTipoVehiculos
    {
        public DataTable TraerTipoVehiculos()
        {
            using (DataTable dt = new DataTable())
            {
                using (SqlConnection cn = new
                      SqlConnection(ClasesBase.Properties.Settings.Default.conexion))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter("Select * from TipoVehiculo", cn))
                    {
                        da.Fill(dt);
                    }
                }
                return dt;
            }
}

        public static TipoVehiculo TraerUNTipoVehiculo(int codigo)
        {

            //conectar la conexion
            SqlConnection cnn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
            //2 paso crear el comando
            SqlCommand cmd = new SqlCommand();
            //3segundo configurar el 
            cmd.CommandText = @"select * from TipoVehiculo
                                where tvcodigo = @codigo";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = cnn;
            cmd.Parameters.AddWithValue("@codigo", codigo);

            TipoVehiculo oTipoV = null;

            cnn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read() == true)
            {
                oTipoV = new TipoVehiculo();
                
                oTipoV.Descripcion= (string)reader["descripcion"];
                oTipoV.Tarifa = (int)reader["tarifa"];
                oTipoV.Imagen = (string)reader["imagen"];
                oTipoV.TVCodigo1 = (int)reader["tvcodigo"];
            }
            cnn.Close();


            return oTipoV;
        }




        public static void guardarTipoV(TipoVehiculo oTipoVehiculo)
        {
            //conectar la conexion
            SqlConnection cnn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
            //2 paso crear el comando
            SqlCommand cmd = new SqlCommand();
            //3segundo configurar el comando

            cmd.CommandText = @"insert into TipoVehiculo
                                (descripcion, tarifa, imagen)
                                 values
                                (@d, @t, @i)";

            cmd.CommandType = CommandType.Text;
            cmd.Connection = cnn;
            cmd.Parameters.AddWithValue("@d", oTipoVehiculo.Descripcion);
            cmd.Parameters.AddWithValue("@t", oTipoVehiculo.Tarifa);
            cmd.Parameters.AddWithValue("@i", oTipoVehiculo.Imagen);



            //4to paso ejecutar la intrsiccion SQL
            cnn.Open();
            cmd.ExecuteNonQuery();
            cnn.Close();

        }
    }
}
