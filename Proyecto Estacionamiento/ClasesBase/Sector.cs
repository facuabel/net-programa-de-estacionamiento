﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClasesBase
{
    public class Sector
    {
        private int sectorCodigo;
        private string descripcion;
        private int zonaCodigo;

        public int ZonaCodigo
        {
            get { return zonaCodigo; }
            set { zonaCodigo = value; }
        }

        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }
        private string identificador;

        public string Identificador
        {
            get { return identificador; }
            set { identificador = value; }
        }
        private bool habilitado;

        public bool Habilitado
        {
            get { return habilitado; }
            set { habilitado = value; }
        }

        public int SectorCodigo
        {
            get { return sectorCodigo; }
            set { sectorCodigo = value; }
        }


    }
}
