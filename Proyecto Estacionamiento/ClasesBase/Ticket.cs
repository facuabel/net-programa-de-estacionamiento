﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClasesBase
{
    public class Ticket
    {
        private int ticketNro;

        public int TicketNro
        {
            get { return ticketNro; }
            set { ticketNro = value; }
        }
        private DateTime fechaHoraEnt;

        public DateTime FechaHoraEnt
        {
            get { return fechaHoraEnt; }
            set { fechaHoraEnt = value; }
        }
        private DateTime fechaHoraSal;

        public DateTime FechaHoraSal
        {
            get { return fechaHoraSal; }
            set { fechaHoraSal = value; }
        }
        private int clienteDNI;

        public int ClienteDNI
        {
            get { return clienteDNI; }
            set { clienteDNI = value; }
        }
        private int TVCodigo;

        public int TVCodigo1
        {
            get { return TVCodigo; }
            set { TVCodigo = value; }
        }
        private string patente;

        public string Patente
        {
            get { return patente; }
            set { patente = value; }
        }
        private int sectorCodigo;

        public int SectorCodigo
        {
            get { return sectorCodigo; }
            set { sectorCodigo = value; }
        }
        private string duracion;

public string Duracion
{
  get { return duracion; }
  set { duracion = value; }
}

      
        private double tarifa;

        public double Tarifa
        {
            get { return tarifa; }
            set { tarifa = value; }
        }
        private double total;

        public double Total
        {
            get { return total; }
            set { total = value; }
        }

        private string UserName;

        public string UserName1
        {
            get { return UserName; }
            set { UserName = value; }
        }
    }
}
