﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.ObjectModel;

namespace ClasesBase
{
    public class TrabajarUsuarios
    {
        public static ObservableCollection<Usuario> TraerUsuarios()
        {
            //conectar la conexion
            SqlConnection cnn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
            //2 paso crear el comando
            SqlCommand cmd = new SqlCommand();
            //3segundo configurar el 
            cmd.CommandText = "select * from Usuarios";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = cnn;

            ObservableCollection<Usuario> Lista = new ObservableCollection<Usuario>();

            cnn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read() == true)
            {
                Usuario oUsuario = new Usuario();

                oUsuario.Apellido = (string)reader["Apellido"];
                oUsuario.Nombre = (string)reader["Nombre"];
                oUsuario.UserName = (string)reader["UserName"];
                oUsuario.Rol = (string)reader["Rol"];
                oUsuario.Password = (string)reader["Password"];

                Lista.Add(oUsuario);
            }
            cnn.Close();


            return Lista;
        }

        public static void EliminarUsuario(string userN)
        {
            //conectar la conexion
            SqlConnection cnn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
            //2 paso crear el comando
            SqlCommand cmd = new SqlCommand();
            //3segundo configurar el comando


            cmd.CommandText = @"delete from Usuarios
                                 where UserName = @user";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = cnn;
            cmd.Parameters.AddWithValue("@user", userN);


            cnn.Open();
            cmd.ExecuteNonQuery();
            cnn.Close();

        }

        public static void ModificarUsuario(Usuario oUsuario)
        {

            //conectar la conexion
            SqlConnection cnn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
            //2 paso crear el comando
            SqlCommand cmd = new SqlCommand();
            //3segundo configurar el 
            cmd.CommandText = @"update Usuarios set Apellido =@a, Nombre = @n, Rol = @r, UserName = @u, Password = @p
                                 where UserName = @u";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = cnn;
            cmd.Parameters.AddWithValue("@a", oUsuario.Apellido);
            cmd.Parameters.AddWithValue("@n", oUsuario.Nombre);
            cmd.Parameters.AddWithValue("@r", oUsuario.Rol);
            cmd.Parameters.AddWithValue("@u", oUsuario.UserName);
            cmd.Parameters.AddWithValue("@p", oUsuario.Password);

            cnn.Open();
            cmd.ExecuteNonQuery();
            cnn.Close();


        }


        public static void AgregarUsuario(Usuario usuarioNuevo) {

            //conectar la conexion
            SqlConnection cnn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
            //2 paso crear el comando
            SqlCommand cmd = new SqlCommand();
            //3segundo configurar el comando

            cmd.CommandText = @"insert into Usuarios
                                (Apellido, Nombre, Rol, UserName, Password)
                                 values
                                (@a, @n, @r, @u, @p)";

            cmd.CommandType = CommandType.Text;
            cmd.Connection = cnn;
            cmd.Parameters.AddWithValue("@a", usuarioNuevo.Apellido);
            cmd.Parameters.AddWithValue("@n", usuarioNuevo.Nombre);
            cmd.Parameters.AddWithValue("@r", usuarioNuevo.Rol);
            cmd.Parameters.AddWithValue("@u", usuarioNuevo.UserName);
            cmd.Parameters.AddWithValue("@p", usuarioNuevo.Password);



            //4to paso ejecutar la intrsiccion SQL
            cnn.Open();
            cmd.ExecuteNonQuery();
            cnn.Close();

        }



        public static Usuario traerUnUsuario(string userN)
        {
            //conectar la conexion
            SqlConnection cnn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
            //2 paso crear el comando
            SqlCommand cmd = new SqlCommand();
            //3segundo configurar el 
            cmd.CommandText = @"select * from Usuarios where UserName = @userN";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = cnn;
            cmd.Parameters.AddWithValue("@userN", userN);

            Usuario user = null;

            cnn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read() == true)
            {
                user = new Usuario();

                user.Apellido = (string)reader["Apellido"];
                user.Nombre = (string)reader["Nombre"];

            }
            cnn.Close();


            return user; 
        }

        public static Usuario buscarUsuario(string userN, string pas)
        {
            //conectar la conexion
            SqlConnection cnn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
            //2 paso crear el comando
            SqlCommand cmd = new SqlCommand();
            //3segundo configurar el 
            cmd.CommandText = @"select * from Usuarios
                                where UserName = @userN and Password = @pas";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = cnn;
            cmd.Parameters.AddWithValue("@userN", userN);
            cmd.Parameters.AddWithValue("@pas",pas);

            Usuario user = null;

            cnn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read() == true)
            {
                user = new Usuario();
                user.Apellido = (string)reader["Apellido"];
                user.Nombre = (string)reader["Nombre"];
                user.Rol = (string)reader["Rol"];
                user.UserName = (string)reader["UserName"];
                user.Password = (string)reader["Password"];
            }
            cnn.Close();


            return user;
        }
    }
}
